from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models.signals import m2m_changed

class Country(models.Model):    
    _id = models.AutoField(primary_key=True)
    country = models.CharField(max_length=150)
    def __unicode__(self):
        return self.country
    class Meta:
        verbose_name_plural = "Countries"
        
class UserProfile(models.Model):
    country = models.ManyToManyField(Country, related_name="country_details")
    user = models.ForeignKey(User, unique=True)
    def __unicode__(self):
        return self.user.username
    class Meta:
        verbose_name_plural = "Admin Profiles"

# class Details(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     country = models.ManyToManyField(Country, related_name="country_details")
    

class City(models.Model):   
    _id =  models.AutoField(primary_key=True)
    country_id = models.ForeignKey(Country)
    city = models.CharField(max_length=150)
    def __unicode__(self):
        return self.city
    class Meta:
        verbose_name_plural = "cities"

class Doctor_profile(models.Model):
    _id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    password = models.CharField(max_length=32)
    email = models.EmailField(unique=True)
    new_email = models.EmailField(blank=True, null=True)
    mobile = models.CharField(max_length=150, unique=True)
    country_id = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    profile_picture = models.ImageField(blank=True, null=True)
    privacy_setting = models.CharField(choices = (
                                ("hidden", ("hidden")),
                                ("maybe", ("maybe")),
                                ("show", ("show")))
                                 ,default="maybe",max_length=100)
    designation = models.CharField(max_length=150, blank=True, null=True)
    email_verified = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=150, blank=True, null=True)
    def __unicode__(self):
        return self.email +" | " + self.name +" | " + self.country_id.country + " | " + self.city.city
    class Meta:
        ordering = ['name']
        verbose_name_plural = "Doctor Profiles"

class GcmDevice(models.Model) :
    _id = models.AutoField(primary_key=True)
    ANDROID = 1
    IPHONE = 2
    CHROME = 3
    OTHER = 4
    DEVICE_CHOICES = ( (ANDROID, 'Android'), (IPHONE, 'iPhone') , (CHROME,'Chrome'), (OTHER,'Others'))
    device_id = models.CharField(max_length = 255, default='')
    device_type = models.SmallIntegerField(choices = DEVICE_CHOICES)
    user = models.ForeignKey(Doctor_profile, null = True, blank=True)
    is_active = models.BooleanField(default=True)
    def __unicode__(self):
        return self.user.name+" | "+self.user.country_id.country
    class Meta:
        verbose_name_plural = "Devices registered for push notifications."

class Notification(models.Model):
    _id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=150)
    content = models.CharField(max_length=150)
    targeted_users = models.ManyToManyField(Doctor_profile, related_name="doctor_notifications")
    country = models.ManyToManyField(Country, related_name="countries_notifications", null=True)
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Notifications - targetted user (Sorting in assigning in admin panel)"

class Access_Code(models.Model):
    _id = models.AutoField(primary_key=True)
    user_id = models.OneToOneField(Doctor_profile, blank=True, null=True)
    codes = models.CharField(max_length=150, unique=True)
    Choices = (
        (0, "Disabled"),
        (1, "Activated")
    )
    status = models.IntegerField(default=1, choices = Choices)
    def __unicode__(self):
        return self.codes
    class Meta:
        verbose_name_plural = "Access Codes for Users"

class Access_token(models.Model):
    user_id = models.ForeignKey(Doctor_profile)
    token_value = models.CharField(max_length=150)
    expiry = models.DateTimeField()

class Event(models.Model):
    created_by = models.ForeignKey(User, null=True, blank=True)
    event_id = models.AutoField(primary_key=True)
    date = models.DateField()
    title = models.CharField(max_length=150)
    description = models.TextField(max_length=500, blank=True, null=True)
    venue = models.CharField(max_length=150)
    timings = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    speaker = models.CharField(max_length=150, blank=True, null=True)
    country = models.ManyToManyField(Country, related_name="countries")
    footfall = models.IntegerField(default=0)
    attending_users = models.ManyToManyField(Doctor_profile, related_name="events", blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.title

class Connect(models.Model):
    _id = models.AutoField(primary_key=True)
    sender_id = models.ForeignKey(Doctor_profile, related_name='sender')
    receiver_id = models.ForeignKey(Doctor_profile, related_name='receiver')
    Choices = (
        (0, "Pending"),
        (1, "Accepted"),
        (2, "Denied")
    )
    status = models.IntegerField(default=1, choices = Choices)
    created_at = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.sender_id.name
    class Meta:
        verbose_name_plural = "Connect"

class Reading_Material(models.Model):
    created_by = models.ForeignKey(User, null=True, blank=True)
    _id = models.AutoField(primary_key=True)
    heading = models.CharField(max_length=150)
    country = models.ManyToManyField(Country, related_name="countries_reading")
    type_of_material = models.CharField(choices = (
                                    ("reviews", ("reviews")),
                                    ("journals", ("journals")),
                                    ("research paper", ("research paper")),
                                    ("PI", ("PI")),
                                )
                                 ,default="reading material",max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    pdf_data = models.FileField (upload_to = "pdfs", max_length=20000, blank=True, null=True)
    targeted_users = models.ManyToManyField(Doctor_profile, blank=True, null=True)
    def __unicode__(self):
        return self.heading
    class Meta:
        verbose_name_plural = "Reading Material"

class Contactus_categories(models.Model):
    _id = models.AutoField(primary_key=True)
    Category = models.CharField(max_length=100, null=True)
    def __unicode__(self):
        return self.Category
    class Meta:
        verbose_name_plural = "Contact Us Categories"

class Contactus(models.Model):
    _id = models.AutoField(primary_key=True)
    created_by = models.ForeignKey(User, null=True)
    Name = models.CharField(max_length=100, null=True)
    EmailId = models.CharField(max_length=100, null=True)
    country = models.ManyToManyField(Country, null=True)
    Role = models.ManyToManyField(Contactus_categories, related_name="contact_us_role")
    Number_of_people_contacted = models.IntegerField(default=0)
    people = models.ManyToManyField(Doctor_profile, related_name="people", blank=True, null=True)
    def __unicode__(self):
        return self.EmailId or u''
    class Meta:
        verbose_name_plural = "Contact Us Section"

class Contact_Queries(models.Model):
    Contact = models.ForeignKey(Contactus)
    Query_type = models.ForeignKey(Contactus_categories)
    Doctor_user = models.ForeignKey(Doctor_profile) 
    country = models.ManyToManyField(Country, blank=True, null=True)
    def __unicode__(self):
        return self.Query_type.Category
    class Meta:
        verbose_name_plural = "Contact Queries"

class Testimonial(models.Model):
    _id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(Doctor_profile) 
    testimonial_content = models.TextField(max_length=150)
    status = models.CharField(choices = (
                                ("approved", ("approved")),
                                ("not approved", ("not approved")))
                                 ,default="not approved",max_length=100)
    timestamp = models.DateTimeField(auto_now_add=True)
    country = models.ForeignKey(Country, null=True)
    def __unicode__(self):
        return self.testimonial_content+"-"+self.status
    class Meta:
        verbose_name_plural = "Testimonials"

class Fixed_testimonial(models.Model):
    _id = models.AutoField(primary_key=True)
    testimonial_content = models.CharField(max_length=250)
    name = models.CharField(max_length=150)
    designation = models.CharField(max_length=150)
    timestamp = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.testimonial_content+"-"+self.name
    class Meta:
        verbose_name_plural = "Testimonials - Fixed"



class Survey_question(models.Model):    
    _id = models.AutoField(primary_key=True)
    created_by = models.ForeignKey(User, null=True)
    question = models.CharField(max_length=150)
    # country2 = models.ForeignKey(Country, null=True)
    country = models.ManyToManyField(Country, null=True)
    targeted_users = models.ManyToManyField(Doctor_profile)
    def __unicode__(self):
        return self.question
    class Meta:
        verbose_name_plural = "Survey Questions"

class Survey_values(models.Model):
    survey_id = models.ForeignKey(Survey_question) 
    option = models.CharField(max_length=150, unique=True)
    def __unicode__(self):
        return self.option
    class Meta:
        verbose_name_plural = "Survey Options"

class Survey_Responses(models.Model):
    user_id = models.ForeignKey(Doctor_profile) 
    question = models.ForeignKey(Survey_question)
    value = models.ForeignKey(Survey_values)  
    country = models.ManyToManyField(Country, null=True)
    def __unicode__(self):
        return self.user_id
    class Meta:
        verbose_name_plural = "Survey Responses"
    


# class User_specific_notification(models.Model):
#     notification_id = models.ForeignKey(Notification)
#     filter_type = models.CharField(max_length=150)
#     filter_id = models.CharField(max_length=150)
#     class Meta:
#         verbose_name_plural = "User Specific Notifications"

# class Permissions(models.Model):
#     User = models.ForeignKey(Details)
#     Categories = (
#         ("1", "Events"),
#         ("2", "Surveys"),
#         ("3", "Literature")
#     )
#     Power = (
#         ("1", "Add"),
#         ("2", "Edit"),
#         ("3", "Read"),
#         ("4", "Delete"),
#     )
#     Categories = models.CharField(max_length=100, choices = Categories, default="Categories")
#     Power = models.CharField(max_length=100, choices = Power, default="Power")
#     def __unicode__(self):
#         return self.User.user.username
    