# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0004_remove_survey_question_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_question',
            name='country',
            field=models.ForeignKey(default=1, to='adminportal.Country'),
            preserve_default=False,
        ),
    ]
