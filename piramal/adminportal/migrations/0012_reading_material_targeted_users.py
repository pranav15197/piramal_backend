# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0011_survey_responses_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='reading_material',
            name='targeted_users',
            field=models.ManyToManyField(to='adminportal.Doctor_profile', null=True, blank=True),
            preserve_default=True,
        ),
    ]
