# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0002_contact_queries'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact_queries',
            name='Doctor_user',
            field=models.ForeignKey(default=1, to='adminportal.Doctor_profile'),
            preserve_default=False,
        ),
    ]
