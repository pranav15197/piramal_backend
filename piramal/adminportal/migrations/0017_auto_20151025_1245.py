# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0016_auto_20151025_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='access_token',
            name='expiry',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='contact_queries',
            name='country',
            field=models.ManyToManyField(to=b'adminportal.Country', null=True, blank=True),
        ),
    ]
