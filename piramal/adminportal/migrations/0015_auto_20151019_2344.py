# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0014_auto_20151019_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactus',
            name='country',
            field=models.ManyToManyField(to=b'adminportal.Country', null=True),
        ),
    ]
