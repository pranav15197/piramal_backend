# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0015_auto_20151019_2344'),
    ]

    operations = [
        migrations.AlterField(
            model_name='access_token',
            name='expiry',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
