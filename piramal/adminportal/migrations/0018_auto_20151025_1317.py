# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0017_auto_20151025_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gcmdevice',
            name='device_id',
            field=models.CharField(default=b'', max_length=255),
        ),
    ]
