# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0010_auto_20151016_2331'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_responses',
            name='country',
            field=models.ManyToManyField(to='adminportal.Country', null=True),
            preserve_default=True,
        ),
    ]
