# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0005_survey_question_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='testimonial',
            name='country',
            field=models.ForeignKey(to='adminportal.Country', null=True),
            preserve_default=True,
        ),
    ]
