# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0007_notification_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_question',
            name='country',
            field=models.ForeignKey(to='adminportal.Country', null=True),
        ),
    ]
