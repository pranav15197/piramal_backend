# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Access_Code',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('codes', models.CharField(unique=True, max_length=150)),
                ('status', models.IntegerField(default=1, choices=[(0, b'Disabled'), (1, b'Activated')])),
            ],
            options={
                'verbose_name_plural': 'Access Codes for Users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Access_token',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token_value', models.CharField(max_length=150)),
                ('expiry', models.CharField(max_length=150)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('city', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name_plural': 'cities',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Connect',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('status', models.IntegerField(default=1, choices=[(0, b'Pending'), (1, b'Accepted'), (2, b'Denied')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Connect',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contactus',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('Name', models.CharField(max_length=100, null=True)),
                ('EmailId', models.CharField(max_length=100, null=True)),
                ('Number_of_people_contacted', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'Contact Us Section',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contactus_categories',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('Category', models.CharField(max_length=100, null=True)),
            ],
            options={
                'verbose_name_plural': 'Contact Us Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('country', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Doctor_profile',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('password', models.CharField(max_length=32)),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('new_email', models.EmailField(max_length=75, null=True, blank=True)),
                ('mobile', models.CharField(unique=True, max_length=150)),
                ('profile_picture', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('privacy_setting', models.CharField(default=b'maybe', max_length=100, choices=[(b'hidden', b'hidden'), (b'maybe', b'maybe'), (b'show', b'show')])),
                ('designation', models.CharField(max_length=150, null=True, blank=True)),
                ('email_verified', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('code', models.CharField(max_length=150, null=True, blank=True)),
                ('city', models.ForeignKey(to='adminportal.City')),
                ('country_id', models.ForeignKey(to='adminportal.Country')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name_plural': 'Doctor Profiles',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('event_id', models.AutoField(serialize=False, primary_key=True)),
                ('date', models.DateField()),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField(max_length=500, null=True, blank=True)),
                ('venue', models.CharField(max_length=150)),
                ('timings', models.TimeField(null=True, blank=True)),
                ('speaker', models.CharField(max_length=150, null=True, blank=True)),
                ('footfall', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('attending_users', models.ManyToManyField(related_name=b'events', null=True, to='adminportal.Doctor_profile', blank=True)),
                ('country', models.ManyToManyField(related_name=b'countries', to='adminportal.Country')),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Fixed_testimonial',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('testimonial_content', models.CharField(max_length=250)),
                ('name', models.CharField(max_length=150)),
                ('designation', models.CharField(max_length=150)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Testimonials - Fixed',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GcmDevice',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('device_id', models.CharField(default=b'', unique=True, max_length=255)),
                ('device_type', models.SmallIntegerField(choices=[(1, b'Android'), (2, b'iPhone'), (3, b'Chrome'), (4, b'Others')])),
                ('is_active', models.BooleanField(default=True)),
                ('user', models.ForeignKey(blank=True, to='adminportal.Doctor_profile', null=True)),
            ],
            options={
                'verbose_name_plural': 'Devices registered for push notifications.',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=150)),
                ('content', models.CharField(max_length=150)),
                ('targeted_users', models.ManyToManyField(related_name=b'doctor_notifications', to='adminportal.Doctor_profile')),
            ],
            options={
                'verbose_name_plural': 'Notifications - targetted user (Sorting in assigning in admin panel)',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reading_Material',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('heading', models.CharField(max_length=150)),
                ('type_of_material', models.CharField(default=b'reading material', max_length=30, choices=[(b'reviews', b'reviews'), (b'journals', b'journals'), (b'research paper', b'research paper'), (b'PI', b'PI')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('pdf_data', models.FileField(max_length=20000, null=True, upload_to=b'pdfs', blank=True)),
                ('country', models.ManyToManyField(related_name=b'countries_reading', to='adminportal.Country')),
                ('created_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Reading Material',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Survey_question',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('question', models.CharField(max_length=150)),
                ('country', models.ForeignKey(to='adminportal.Country')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('targeted_users', models.ManyToManyField(to='adminportal.Doctor_profile')),
            ],
            options={
                'verbose_name_plural': 'Survey Questions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Survey_Responses',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.ForeignKey(to='adminportal.Survey_question')),
                ('user_id', models.ForeignKey(to='adminportal.Doctor_profile')),
            ],
            options={
                'verbose_name_plural': 'Survey Responses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Survey_values',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option', models.CharField(unique=True, max_length=150)),
                ('survey_id', models.ForeignKey(to='adminportal.Survey_question')),
            ],
            options={
                'verbose_name_plural': 'Survey Options',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('testimonial_content', models.TextField(max_length=150)),
                ('status', models.CharField(default=b'not approved', max_length=100, choices=[(b'approved', b'approved'), (b'not approved', b'not approved')])),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('user_id', models.ForeignKey(to='adminportal.Doctor_profile')),
            ],
            options={
                'verbose_name_plural': 'Testimonials',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.ManyToManyField(related_name=b'country_details', to='adminportal.Country')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Admin Profiles',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='survey_responses',
            name='value',
            field=models.ForeignKey(to='adminportal.Survey_values'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactus',
            name='Country',
            field=models.ManyToManyField(related_name=b'countries_contact_us', to='adminportal.Country'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactus',
            name='Role',
            field=models.ManyToManyField(related_name=b'contact_us_role', to='adminportal.Contactus_categories'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactus',
            name='people',
            field=models.ManyToManyField(related_name=b'people', null=True, to='adminportal.Doctor_profile', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='connect',
            name='receiver_id',
            field=models.ForeignKey(related_name=b'receiver', to='adminportal.Doctor_profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='connect',
            name='sender_id',
            field=models.ForeignKey(related_name=b'sender', to='adminportal.Doctor_profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='city',
            name='country_id',
            field=models.ForeignKey(to='adminportal.Country'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='access_token',
            name='user_id',
            field=models.ForeignKey(to='adminportal.Doctor_profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='access_code',
            name='user_id',
            field=models.OneToOneField(null=True, blank=True, to='adminportal.Doctor_profile'),
            preserve_default=True,
        ),
    ]
