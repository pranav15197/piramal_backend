# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('adminportal', '0012_reading_material_targeted_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact_queries',
            name='country',
            field=models.ManyToManyField(to='adminportal.Country', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactus',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
