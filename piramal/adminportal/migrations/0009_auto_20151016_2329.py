# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0008_auto_20151016_2329'),
    ]

    operations = [
        migrations.RenameField(
            model_name='survey_question',
            old_name='country',
            new_name='country2',
        ),
    ]
