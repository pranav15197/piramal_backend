# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0003_contact_queries_doctor_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_question',
            name='country',
        ),
    ]
