# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0009_auto_20151016_2329'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_question',
            name='country2',
        ),
        migrations.AddField(
            model_name='survey_question',
            name='country',
            field=models.ManyToManyField(to='adminportal.Country', null=True),
            preserve_default=True,
        ),
    ]
