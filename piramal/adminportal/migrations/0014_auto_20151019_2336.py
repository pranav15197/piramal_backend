# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0013_auto_20151017_1519'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contactus',
            old_name='Country',
            new_name='country',
        ),
    ]
