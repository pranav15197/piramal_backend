# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact_Queries',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Contact', models.ForeignKey(to='adminportal.Contactus')),
                ('Query_type', models.ForeignKey(to='adminportal.Contactus_categories')),
            ],
            options={
                'verbose_name_plural': 'Contact Queries',
            },
            bases=(models.Model,),
        ),
    ]
