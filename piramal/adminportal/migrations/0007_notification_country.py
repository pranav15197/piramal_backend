# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adminportal', '0006_testimonial_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='country',
            field=models.ManyToManyField(related_name=b'countries_notifications', null=True, to='adminportal.Country'),
            preserve_default=True,
        ),
    ]
