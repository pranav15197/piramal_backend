from django import forms
from adminportal.models import *

class UserMakeForm(forms.ModelForm):
    name = forms.CharField()
    password = forms.CharField()
    email = forms.CharField()
    mobile = forms.CharField()
    # profile_picture = forms.ImageField()
    class Meta:
        model = Doctor_profile
        fields = ('name', 'password', 'email', 'mobile')
