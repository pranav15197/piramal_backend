from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from django.forms import CheckboxSelectMultiple
from adminportal.models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.conf import settings

from gcm import GCM
gcm = GCM(settings.GCM_APIKEY)




###########################Ending Notification Function########################

def send_notification( title, content, type_of_notification, reg_device_id ):
    if reg_device_id :
        data = {'title': title, 'message': content, 'type_of_notification': type_of_notification}
        reg_id = [reg_device_id]
        response = gcm.json_request(registration_ids=reg_id, data=data)
        print response

################################################################################












#########################################Reading Material########################################

class Reading_Material_Admin(admin.ModelAdmin):
    list_display = ('heading', 'type_of_material', 'pdf_data', 'country', 'targeted_users')
    list_editable = ('heading', 'type_of_material', 'pdf_data')
    list_filter = ['type_of_material']
    search_fields = ['heading']
    filter_horizontal = ('country', 'targeted_users')
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                if obj.created_by == request.user:
                    return True
                else : 
                    return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return []
            elif obj.created_by == request.user :
                return []
            else:
                return ['heading', 'type_of_material', 'pdf_data', 'country']
        else : 
            return []
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []  
        self.exclude.append('created_by')
        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        form = super(Reading_Material_Admin, self).get_form(request, obj, **kwargs)
        # print form.base_fields['country'].queryset
        if obj :
            admin = UserProfile.objects.get(user = request.user)
            admin_countries = admin.country.all()
            doctors_list_to_be_displayed = Doctor_profile.objects.filter(country_id=admin_countries)

            form.base_fields['targeted_users'].queryset = doctors_list_to_be_displayed
            form.base_fields['country'].queryset = country_cumulative_list
            return form
        else : 
            admin = UserProfile.objects.get(user = request.user)
            admin_countries = admin.country.all()
            doctors_list_to_be_displayed = Doctor_profile.objects.filter(country_id=admin_countries)
            print doctors_list_to_be_displayed
            form.base_fields['targeted_users'].queryset = doctors_list_to_be_displayed
            form.base_fields['country'].queryset = country_cumulative_list
            return form
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(Reading_Material_Admin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            country_cumulative_list = qsc.country.all()
            return qs.filter(country=country_cumulative_list).distinct()
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
admin.site.register(Reading_Material, Reading_Material_Admin) 
#########################################################################################

















#######################Contact Us, Contact_Us_Categories, Contact Queries######################
class Contactus_categories_resource(resources.ModelResource):
    class Meta:
        model = Contactus_categories
        import_id_fields = ('_id',)
        fields = ('_id', 'Category',)
class Contactus_categoriesAdmin(ImportExportModelAdmin):
    list_display = ['Category']
    resource_class = Contactus_categories_resource
admin.site.register(Contactus_categories, Contactus_categoriesAdmin)


class Contactus_resource(resources.ModelResource):
    class Meta:
        model = Contactus
        import_id_fields = ('_id',)
        fields = ('_id', 'Name', 'EmailId', 'country', 'Role')

class ContactusAdmin(ImportExportModelAdmin):
    list_display = ('Name', 'EmailId', 'country', 'Role', 'Number_of_people_contacted', 'created_by')
    exclude = []
    readonly_fields = ['Number_of_people_contacted', 'people', 'created_by']
    filter_horizontal = ('country', 'Role')
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                if obj.created_by == request.user:
                    return True
                else : 
                    return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj :
            if request.user.is_superuser:
                return ["Number_of_people_contacted", 'created_by', 'people']
            elif obj.created_by == request.user :
                return ["Number_of_people_contacted", 'created_by', 'people']
            else:
                return ['Name', 'EmailId', 'country', 'Role', 'people']
        else :
            return ["Number_of_people_contacted", 'created_by', 'people']
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = [] 
        
        self.exclude.append('Number_of_people_contacted') 
        if not request.user.is_superuser:
            if obj:
                if not obj.created_by == request.user :
                    self.exclude.append('created_by')
                    self.exclude.append('people')
            else : 
                self.exclude.append('people')

        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        all_countries = Country.objects.all()
        form = super(ContactusAdmin, self).get_form(request, obj, **kwargs)
        if obj :
            if request.user.is_superuser: 
                form.base_fields['country'].queryset = country_cumulative_list
            elif obj.created_by == request.user :
                form.base_fields['country'].queryset = country_cumulative_list
            else:
                print "Sds"
        else : 
            if request.user.is_superuser: 
                form.base_fields['country'].queryset = all_countries
            else:
                print "Sds"
                print country_cumulative_list
                form.base_fields['country'].queryset = country_cumulative_list
        return form
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(ContactusAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            print qsc.country.all()
            country_cumulative_list = qsc.country.all()
            print qs.filter(country=country_cumulative_list).distinct()
        return qs.filter(country=country_cumulative_list).distinct()
    resource_class = Contactus_resource
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
    
admin.site.register(Contactus, ContactusAdmin)


class Contact_QueriesAdmin(admin.ModelAdmin):
    list_display = ("Contact", 'Query_type', 'Doctor_user')
    filter_horizontal = ('country',)
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj :
            if request.user.is_superuser:
                return ["Contact", 'Query_type', 'Doctor_user', 'country']
            else:
                return ["Contact", 'Query_type', 'Doctor_user', 'country']
        else :
            return ["Contact", 'Query_type', 'Doctor_user', 'country']
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(Contact_QueriesAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            print qsc.country.all()
            country_cumulative_list = qsc.country.all()
            print qs.filter(country=country_cumulative_list).distinct()
        return qs.filter(country=country_cumulative_list).distinct()

admin.site.register(Contact_Queries, Contact_QueriesAdmin)  
# admin.site.register(Contact_Queries)

###############################################################












########################Event Admin#####################
class EventAdmin_resource(resources.ModelResource):
    class Meta:
        model = Event
        import_id_fields = ('event_id',)
        fields = ('event_id', 'date', 'title', 'description', 'venue', 'timings', 'venue', 'speaker', 'country')



class EventAdmin(ImportExportModelAdmin):
    list_display = ['created_by', 'title', 'country', 'venue', 'footfall', 'speaker']
    exclude = ['attending_users']
    readonly_fields = ['footfall', 'attending_users']
    filter_horizontal = ('country',)
    resource_class = EventAdmin_resource
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                if obj.created_by == request.user:
                    return True
                else : 
                    return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return ['footfall', 'attending_users']
            elif obj.created_by == request.user :
                return ['footfall', 'attending_users']
            else:
                return ['created_by', 'date', 'title', 'description', 'venue', 'timings', 'speaker', 'country', 'footfall', 'attending_users']
        else : 
            return ['footfall', 'attending_users']
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []  
        self.exclude.append('created_by')
        self.exclude.append('attending_users') 
        # self.country = country_cumulative_list
        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        form = super(EventAdmin, self).get_form(request, obj, **kwargs)
        # print form.base_fields['country'].queryset
        if obj :
            # print obj.country.all()
            # print len(obj.country.all())
            if len(obj.country.all())<20 :
                if request.user.is_superuser: 
                    form.base_fields['country'].queryset = country_cumulative_list
                    return form
                elif obj.created_by == request.user :
                    form.base_fields['country'].queryset = country_cumulative_list
                    return form
                else:
                    # form.base_fields['country'].queryset = country_cumulative_list
                    return form
            else :
                form.base_fields['country'].queryset = country_cumulative_list
                return form
        else : 
            form.base_fields['country'].queryset = country_cumulative_list
            return form
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(EventAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            # print qsc.country.all()
            country_cumulative_list = qsc.country.all()
            # print qs.filter(country=country_cumulative_list).distinct()
        return qs.filter(country=country_cumulative_list).distinct()
    def save_model(self, request, obj, form, change):
        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        # print country_cumulative_list
        obj.save()
        print form.cleaned_data['country']
        users_to_notify = []
        for specific_country in form.cleaned_data['country']:
            users_to_send_notification = Doctor_profile.objects.filter(country_id=specific_country)
            for unique_user in users_to_send_notification:
                users_to_notify.append(unique_user)
        users_to_notify = list(set(users_to_notify))
        for each_unique_user in users_to_notify:
            title = "New Event Scheduled"
            content = "You have a new event to attend to."
            email = each_unique_user.email
            doc_user = Doctor_profile.objects.get(email=email)
            reg_device_id = GcmDevice.objects.get(user=doc_user, device_type=1).device_id
            send_notification(title, content, "survey", reg_device_id)
admin.site.register(Event, EventAdmin)





























##########################SuperUser#####################
class CityInline(admin.StackedInline):
    model = City
    extra = 1
class CountryInline(admin.StackedInline):
    model = Country
    extra = 0

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    extra = 1

class UserProfileAdmin(admin.ModelAdmin):
    filter_horizontal = ('country',)

admin.site.register(UserProfile, UserProfileAdmin)  

# admin.site.unregister(User)  
# class Admin_profile_Admin(admin.ModelAdmin):
#     inlines = [UserProfileInline]
# admin.site.register(User, Admin_profile_Admin)



class Countryresource(resources.ModelResource):
    class Meta:
        model = Country
        import_id_fields = ('_id',)
        fields = ('_id', 'country',)

class CountryAdmin(ImportExportModelAdmin):
    list_display = ['country']
    resource_class = Countryresource
    inlines = [CityInline]
admin.site.register(Country, CountryAdmin)

class Cityresource(resources.ModelResource):
    class Meta:
        model = City
        import_id_fields = ('_id',)
        fields = ('_id', "city", "country_id")
class CityAdmin(ImportExportModelAdmin):
    list_display = ['city', 'country_name']
    def country_name(self, instance):
        return instance.country_id.country
    resource_class = Cityresource
admin.site.register(City, CityAdmin)


class Doctor_profile_resource(resources.ModelResource):
    class Meta:
        model = Doctor_profile
        import_id_fields = ('_id',)
        fields = ('name', 'email', 'mobile', 'country_id__country', 'city__city', 'designation')

class Doctor_profile_Admin(ImportExportModelAdmin):
    list_display = ('name', 'country_name', 'city', 'designation')
    list_filter = ['country_id', 'city', 'designation']
    # search_fields = ['name', 'country_id', 'city']
    def country_name(self, instance):
            return instance.country_id.country
    resource_class = Doctor_profile_resource
# class Doctor_profile_Admin(admin.ModelAdmin):
    exclude = ('password',)
    fieldsets = (
        (None, {
            'fields': (('name', 'email'), 'new_email', 'mobile', ('country_id', 'city'), 'email_verified')
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('profile_picture', 'privacy_setting', 'designation')
        }),
    )
    def get_actions(self, request):
        #Disable delete
        actions = super(Doctor_profile_Admin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                return False 
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return ['privacy_setting']
            else:
                return ['name', 'email', 'password', 'new_email', 'mobile', 'country_id', 'city', 'profile_picture', 'privacy_setting', 'designation', 'email_verified']
        else : 
            return ['name', 'email', 'password', 'new_email', 'mobile', 'country_id', 'city', 'profile_picture', 'privacy_setting', 'designation', 'email_verified']
    def delete_model(self, request, obj):
        v = Access_Code.objects.get(user_id=obj)
        obj.delete()
        Access_Code.objects.create(codes=v)
    # def get_form(self, request, obj=None, **kwargs):
    #     # self.exclude = []  
    #     # self.exclude.append('created_by')
    #     # self.exclude.append('attending_users') 
    #     # self.country = country_cumulative_list

    #     qsc = UserProfile.objects.get(user = request.user)
    #     country_cumulative_list = qsc.country.all()
    #     form = super(Doctor_profile_Admin, self).get_form(request, obj, **kwargs)

    #     print form.base_fields['country']
    #     form.base_fields['country'].queryset = country_cumulative_list
    #     return form
admin.site.register(Doctor_profile, Doctor_profile_Admin)













class acresource(resources.ModelResource):
    class Meta:
        model = Access_Code
        import_id_fields = ('_id',)
        fields = ('_id', 'codes', 'status',)

class Access_Code_Admin(ImportExportModelAdmin):
    list_display = ['user_id', 'codes', 'status']
    list_filter = ['status']
    search_fields = ['user_id']
    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return ['user_id', 'codes']
        else :
            return []
    def get_actions(self, request):
        #Disable delete
        actions = super(Access_Code_Admin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                return False
            else : 
                return True
    resource_class = acresource
    pass
admin.site.register(Access_Code, Access_Code_Admin)
admin.site.register(Access_token)

admin.site.register(Connect)

class OptionsInline(admin.StackedInline):
    model = Survey_values
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return []
            elif obj.created_by == request.user :
                return []
            else:
                return ['option'] 
        else:
            return []
    extra = 1  

class SurveyQuestionAdmin(admin.ModelAdmin):
    list_display = ['created_by', 'question', 'country']
    filter_horizontal = ('targeted_users', 'country')
    # exclude = ['created_by']
    inlines= [OptionsInline]
    def get_actions(self, request):
        #Disable delete
        actions = super(SurveyQuestionAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                if obj.created_by == request.user:
                    return True
                else : 
                    return False
            else : 
                return True
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []  
        self.exclude.append('created_by')
        self.exclude.append('attending_users') 
        # self.country = country_cumulative_list
        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        form = super(SurveyQuestionAdmin, self).get_form(request, obj, **kwargs)
        # return form
        # print type(Doctor_profile.objects.all())
        # finalist = Doctor_profile.objects.all()
        # print type(finalist)
        # print finalist 
        print "sdsd"
        if not obj :
            if not request.user.is_superuser:
                admin = UserProfile.objects.get(user = request.user)
                admin_countries = admin.country.all()
                doctors_list_to_be_displayed = Doctor_profile.objects.filter(country_id=admin_countries)
                print doctors_list_to_be_displayed
                form.base_fields['targeted_users'].queryset = doctors_list_to_be_displayed
                form.base_fields['country'].queryset = admin_countries
                return form
                # print len(admin_countries)
                # doctors = Doctor_profile.objects.all()
                # final_doctors_list = []
                # for x in doctors:
                #     count = 0
                #     # print str(count)+"count"
                #     for y in admin_countries:
                #         if not x in final_doctors_list :
                #             # print x.country_id.country + "sdsd"+y.country
                #             # print str(count)+"count2"
                #             if str(x.country_id.country) == str(y.country):
                #                 # print "fuckyeahh"
                #                 count = count +1
                #                 final_doctors_list.append(x.email)
                #                 # finalist = Doctor_profile.objects.filter(email=x.email)
                #                 # print final_doctors_list
                #             if count == 0:
                #                 print x.email
                #                 ## remove from query list or convert list into queryset
                #                 # finalist.remove()
                #                 # finalist.remove(email=x.email)
                # print final_doctors_list   
                # #converting final_doctors_list(list) into queryset      
                # # vvv = Doctor_profile.objects.filter(final_doctors_list)
                # # print vvvt
                # final_doctors_list_distinct = list(set(final_doctors_list))
                # print final_doctors_list_distinct
                # doctors_list_to_be_displayed = Doctor_profile.objects.filter(email='bhartiya.shubham08d@gmail.com')
                
            else : 
                return form
        else : 
            return form
            
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return []
            elif obj.created_by == request.user :
                return ['country', 'created_by']
            else:
                return ['question', 'country', 'targeted_users', 'created_by'] 
        else:
            return []
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(SurveyQuestionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            country_cumulative_list = qsc.country.all()
            print qsc
            # print qsc.country.all()
            # country_cumulative_list = qsc.country.all()
            # print qs.filter(country=qsc)
            return qs.filter(country=country_cumulative_list)
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        print form.cleaned_data['targeted_users']
        for target_user in form.cleaned_data['targeted_users']:
            print target_user.email
            print 
            title = "New Survey"
            content = "You have new survey questions  to answer."
            email = target_user.email
            doc_user = Doctor_profile.objects.get(email=email)
            reg_device_id = GcmDevice.objects.get(user=doc_user, device_type=1).device_id
            send_notification(title, content, "survey", reg_device_id)

admin.site.register(Survey_question, SurveyQuestionAdmin)


class SurveyResponseResource(resources.ModelResource):
    class Meta:
        model = Survey_Responses
        import_id_fields = ('user_id',)
        fields = ('user_id__email', 'question__question', 'value__option')

class SurveyResponseAdmin(ImportExportModelAdmin):
    list_display = ('user_id', 'question', 'value', 'country')
    filter_horizontal = ('country')
    resource_class = SurveyResponseResource
    def get_actions(self, request):
        #Disable delete
        actions = super(SurveyResponseAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        if obj :
            # if not request.user.is_superuser:
            #     return False
            # else : 
            return False
    def get_readonly_fields(self, request, obj=None):
        if obj:
            # if request.user.is_superuser: 
            #     return []
            # else:
            return ['user_id', 'question', 'value', 'country'] 
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(SurveyResponseAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            country_cumulative_list = qsc.country.all()
            # print country_cumulative_list
            # # Filtering doctors based on the admins' countries
            # # print qsc.country.all()
            # # country_cumulative_list = qsc.country.all()
            # # print qs.filter(country=qsc)
            return qs.filter(country=country_cumulative_list).distinct()
    # def queryset(self, request):
    #     """Limit Pages to those that belong to the request's user."""
    #     qs = super(SurveyQuestionAdmin, self).get_queryset(request)
    #     if request.user.is_superuser:
    #         return qs
    #     else : 
    #         qsc = UserProfile.objects.get(user = request.user)
    #         country_cumulative_list = qsc.country.all()
    #         print qsc
    #         # print qsc.country.all()
    #         # country_cumulative_list = qsc.country.all()
    #         # print qs.filter(country=qsc)
    #         return qs.filter(country=country_cumulative_list)
admin.site.register(Survey_Responses, SurveyResponseAdmin)















class NotificationsAdmin(admin.ModelAdmin):
    list_display = ('title', 'content')
    fields = ['content', 'title','targeted_users', 'country']
    filter_horizontal = ('targeted_users', 'country')
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return []
            else:
                return ['title', 'content', 'targeted_users', "country"] 
        else:
            return []
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(NotificationsAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            country_cumulative_list = qsc.country.all()
            # print country_cumulative_list
            # # Filtering doctors based on the admins' countries
            # # print qsc.country.all()
            # # country_cumulative_list = qsc.country.all()
            # # print qs.filter(country=qsc)
            return qs.filter(country=country_cumulative_list).distinct()
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = []  
        # self.exclude.append("country")
        qsc = UserProfile.objects.get(user = request.user)
        country_cumulative_list = qsc.country.all()
        form = super(NotificationsAdmin, self).get_form(request, obj, **kwargs)
        print "sdsd"
        if not obj :
            if not request.user.is_superuser:
                admin = UserProfile.objects.get(user = request.user)
                admin_countries = admin.country.all()
                doctors_list_to_be_displayed = Doctor_profile.objects.filter(country_id=admin_countries)
                print doctors_list_to_be_displayed
                form.base_fields['country'].queryset = admin_countries
                form.base_fields['targeted_users'].queryset = doctors_list_to_be_displayed
                return form
            else : 
                return form
        else : 
            return form
    def save_model(self, request, obj, form, change):
        admin = UserProfile.objects.get(user = request.user)
        admin_countries = admin.country.all()
        print admin_countries
        # obj.country = admin_countries
        obj.save()
        print obj._id
        notif___array = Notification.objects.filter(_id=obj._id)
        print notif___array
        for notif in notif___array:
        #     print notif
            notif.country = admin_countries
            # for countryyy in admin_countries:
            #     notif.country.add(countryyy)
            print notif
            notif.save()
        # notif___array.save()
        # print form.cleaned_data['country']
        print form.cleaned_data['targeted_users']
        for target_user in form.cleaned_data['targeted_users']:
            print target_user.email
            title = obj.title
            content = obj.content
            email = target_user.email
            doc_user = Doctor_profile.objects.get(email=email)
            reg_device_id = GcmDevice.objects.get(user=doc_user, device_type=1).device_id
            send_notification(title, content, "global", reg_device_id)
admin.site.register(Notification, NotificationsAdmin)
admin.site.register(GcmDevice)
# admin.site.unregister(gcm)
























class TestimonialResponseAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'testimonial_content', 'status')
    list_filter = ['status']
    search_fields = ['testimonial_content']
    def has_add_permission(self, request):
        if not request.user.is_superuser:
            return False
        else : 
            return True
    def has_delete_permission(self, request, obj=None):
        if obj :
            if not request.user.is_superuser:
                return False
            else : 
                return True
    def get_readonly_fields(self, request, obj=None):
        if obj:
            if request.user.is_superuser: 
                return []
            else:
                return ['user_id', 'country'] 
        else:
            return []
    def queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(TestimonialResponseAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        else : 
            qsc = UserProfile.objects.get(user = request.user)
            country_cumulative_list = qsc.country.all()
            print country_cumulative_list

            # Filtering based on doctors countries
            # print qsc.country.all()
            # country_cumulative_list = qsc.country.all()
            # print qs.filter(country=qsc)
            return qs.filter(country=country_cumulative_list).distinct()
admin.site.register(Testimonial, TestimonialResponseAdmin)

class Fixed_testimonial_resource(resources.ModelResource):
    class Meta:
        model = Fixed_testimonial
        import_id_fields = ('_id',)
        fields = ('_id', 'testimonial_content', 'name', 'designation')

class Fixed_testimonial_Admin(ImportExportModelAdmin):
    list_display = ['testimonial_content', 'name', 'designation']
    resource_class = Fixed_testimonial_resource
admin.site.register(Fixed_testimonial, Fixed_testimonial_Admin)
