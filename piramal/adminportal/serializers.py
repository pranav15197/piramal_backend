from adminportal.models import *

from rest_framework import serializers

# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User_make
#         fields = ('name','mobile', 'email','city','password')
        
class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('_id', 'city','country_id')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor_profile
        # fields = ('city_id','city','country_id')

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('_id', 'country')

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('title', 'content', 'country', 'targeted_users')

# class OptionsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = City
#         fields = ('city_id','city','country_id')