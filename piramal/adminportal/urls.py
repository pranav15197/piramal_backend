from django.conf.urls import include, patterns, url
from adminportal import views

from tastypie.api import Api
# from adminportal.api import *



urlpatterns = patterns('',
        url(r'^email_verify/.*$', views.email_verify, name='email_verify'),
        url(r'^resend_email_verify/.*$', views.resend_email_verify, name='resend_email_verify'),
        url(r'^change_password/.*$', views.change_password, name='change_password'),
        url(r'^forgot_password_email_send/.*$', views.forgot_password_email_send, name='forgot_password_email_send'),
        
		url(r'^$', views.url_redirect, name='url_redirect'),
        url(r'^verify_user/$', views.verify_user, name='verify_user'),
        url(r'^register/$', views.register, name='register'),
        url(r'^login/$', views.login, name='login'),
        url(r'^logout/$', views.logout, name='logout'),
        url(r'^editprofile/$', views.editprofile, name='editprofile'),
        # url(r'^changepassword/$', views.changepassword, name='changepassword'),
        # url(r'^changedp/$', views.changedp, name='changedp'),
        url(r'^addtestimonial/$', views.addtestimonial, name='addtestimonial'),
        url(r'^gettestimonials/$', views.gettestimonials, name='gettestimonials'),
        url(r'^get_fixed_testimonials/$', views.get_fixed_testimonials, name='get_fixed_testimonials'),
        url(r'^contact_request/$', views.contact_request, name='contact_request'),
        url(r'^contact_request_submit/$', views.contact_request_submit, name='contact_request_submit'),

        url(r'^city/$', views.city.as_view(), name='city'),
        url(r'^cityall/$', views.cityall.as_view(), name='cityall'),
        url(r'^country/$', views.country, name='country'),
        # url(r'^access_tokens/$', views.access_tokens, name='access_tokens'),
        url(r'^city_country/$', views.city_country, name='city_country'),
 
        url(r'^getevents/$', views.getevents, name='getevents'),
        url(r'^change_event_status/$', views.change_event_status, name='change_event_status'),

        url(r'^getReadingMaterial/$', views.getReadingMaterial, name='getReadingMaterial'),
        
        url(r'^getdoctors/$', views.getdoctors, name='getdoctors'),
        url(r'^sendrequest/$', views.sendrequest, name='sendrequest'),
        url(r'^acceptrequest/$', views.acceptrequest, name='acceptrequest'),
        url(r'^displaydetails/$', views.displaydetails, name='displaydetails'),
        url(r'^displayrequests/$', views.displayrequests, name='displayrequests'),

        url(r'^getsurveyquestions/$', views.getsurveyquestions, name='getsurveyquestions'),
        url(r'^getsurveyoptions/$', views.getsurveyoptions, name='getsurveyoptions'),
        url(r'^submitsurvey/$', views.submitsurvey, name='submitsurvey'),

        url(r'^getnotification/$', views.getnotification, name='getnotification'),
        url(r'^notif_regid_change/$', views.notif_regid_change, name='notif_regid_change'),
        # url(r'^send_notification/$', views.send_notification, name='send_notification'),
        
    ) 
