from django.shortcuts import render
import time, json
from django.http import HttpResponse
from adminportal.forms import UserMakeForm
from adminportal.models import *
from adminportal.forms import *
# from django.core import serializers  //conflict with the below serializer
from adminportal.serializers import *

import datetime

# from django.utils import simplejson
from django.http import Http404
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from gcm import GCM
gcm = GCM(settings.GCM_APIKEY)
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
import uuid
from django.http import HttpResponsePermanentRedirect
# class country(APIView):
#     def post(self, request, format=None):
#         countries = Country.objects.all()
#         serializer = CountrySerializer(countries, many=True)
#         return Response(serializer.data)
def url_redirect(request):
    return HttpResponsePermanentRedirect("/admin/")

class city(APIView):
    def post(self, request, format=None):
        print request.data['country_id']
        cities = City.objects.filter(country_id=int(request.data['country_id']))
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data)
class cityall(APIView):
    def post(self, request, format=None):
        cities = City.objects.all()
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data)

# class userall(APIView):
#     def post(self, request, format=None):
#         cities = Doctor_profile.objects.all()
#         serializer = UserSerializer(cities, many=True)
#         return Response(serializer.data)

def create_MD5_hash(mystring):
    import hashlib
    hash_object = hashlib.md5(mystring.encode())
    return hash_object.hexdigest()

def verify_login(email, access_token):
    user = Doctor_profile.objects.filter(email=email)
    if not len(user)==0:
        for x in user : 
            correct_access_tokens = Access_token.objects.filter(user_id = x) 
            access_token = Access_token.objects.get(token_value=access_token)
            if access_token in correct_access_tokens:
                # print datetime.datetime.today().strftime('%s')
                # print access_token.expiry.strftime('%s')
                # if datetime.datetime.today().strftime('%s') < access_token.expiry.strftime('%s') :    #comparing epoch values
                return True
                # else:
                #     return False
            else :
                return False
    else: 
        return False

def send_notification( title, content, count,  type_of_notification, reg_device_id ):
    if reg_device_id :
        data = {'title': title, 'message': content, 'type_of_notification': type_of_notification, 'count':count}
        reg_id = [reg_device_id]
        response = gcm.json_request(registration_ids=reg_id, data=data)
        print response


def verify_user(request):
    response_data={}
    user = Doctor_profile.objects.filter(email=request.POST['email'])
    reg_device_id = GcmDevice.objects.get(user=user, device_type=request.POST["device_type"]).device_id
    # if reg_device_id==request.POST['device_id']:
    if Doctor_profile.objects.get(email=request.POST['email']).email_verified ==True:

        # literature
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country_one = Country.objects.get(country=request.POST["country"])
        country_global = Country.objects.get(country="Global")
        materials = Reading_Material.objects.filter(country=country_one)
        materials_global = Reading_Material.objects.filter(country=country_global)
        merged_materials = []
        for m in materials:
            merged_materials.append(m)
        for k in materials_global:
            merged_materials.append(k)

        #events
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country = Country.objects.get(country=request.POST["country"])
        country_global = Country.objects.get(country="global")
        print country_global._id
        events = Event.objects.filter(country__in=[country._id, country_global._id]).order_by('date')
        eventslist = list(set(events))

        # requests
        requests_final_array = []
        receiver = Doctor_profile.objects.get(email=request.POST["email"])
        requests = Connect.objects.filter(receiver_id=receiver, status=0)
        if not len(requests)==0:
            for request in requests:
                if request:
                    print request.sender_id.name
                    doctor = Doctor_profile.objects.get(_id=request.sender_id._id)
                    requests_final_array.append(doctor)  

        #notifications
        notifications_finallist = []
        # user = Doctor_profile.objects.get(email=request.POST["email"])
        Notifications = Notification.objects.all()
        for user_in_notifs in Notifications:
            targeted_users_in_notifs = user_in_notifs.targeted_users.all()
            for user_in_notifss in targeted_users_in_notifs:
                print user_in_notifss.name
                if request.POST["email"]==user_in_notifss.email:
                    notifications_finallist.append(user_in_notifs)

        response_data['result'] = 'success'
        response_data['message'] = 'User email is verified.'
        response_data['count_literature'] = len(merged_materials)
        response_data['count_events'] = len(eventslist)
        response_data['count_connect'] = len(requests_final_array)
        response_data['count_notifications'] = len(notifications_finallist)
    else : 
        response_data['result'] = 'failed'
        response_data['message'] = 'User email is not verified.'
    # else:
    #     response_data['result'] = 'failed'
    #     response_data['message'] = 'User mobile is not registered for gcm.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def country(request):
    response_data={}
    Countries = Country.objects.all()
    response_data=json.dumps([{'_id': country._id, 'country': country.country} for country in Countries ])
    return HttpResponse(response_data, content_type="application/json")

def access_tokens(request):
    response_data={}
    access_tokens = Access_token.objects.all()
    response_data=json.dumps([{'_id': at.user_id._id, 'token_value': at.token_value, 'expiry': str(at.expiry)} for at in access_tokens ])
    return HttpResponse(response_data, content_type="application/json")

def city_country(request):
    response_data={}
    Countries = Country.objects.all()
    cities = City.objects.all()
    for city in cities:
        for country in Countries :
            if country._id==city.country_id._id:
                city.country_name = country.country
                break
            else :
                city.country_name = "null"
    response_data=json.dumps([{'_id': city._id, 'country_id': city.country_id._id, 'city': city.city, 'country': city.country_name} for city in cities ])
    return HttpResponse(response_data, content_type="application/json")

def error_register(request):
    code = Access_Code.objects.get(codes=request.POST["access_code"])
    if code.status == 0:
        return (True, "Used access code")
    if Doctor_profile.objects.filter(email=request.POST['email']).exists():
        return (True, "Email ID already in use")
    if Doctor_profile.objects.filter(mobile=request.POST['mobile']).exists():
        return (True, "Mobile number already in use")
    if not len(str(request.POST['password'])) >= 8:
        return (True, "Password length should be >=8")
    if not "@" in request.POST['email']:
        return (True, "Please enter your correct email id.")
    else : 
        return (False, "")

# def send_registration_confirmation(user):
#     p = user.get_profile()
#     title = "Gsick account confirmation"
#     content = "http://www.gsick.com/confirm/" + str(p.confirmation_code) + "/" + user.username
#     send_mail(title, content, 'no-reply@gsick.com', [user.email], fail_silently=False)

def send_email_mandrill(fromemail, toemail, subject, body) :
    msg = EmailMultiAlternatives(subject=subject, body=body, from_email=fromemail, to=[toemail])
    msg.send()
    response = msg.mandrill_response[0]
    mandrill_id = response['_id']

def email_verify(request):
    response_data={}
    code = request.GET['code']
    email = request.GET['email']
    print str(code) + str(email)
    if Doctor_profile.objects.filter(email=email, code=code).exists():
        Doctor_profile.objects.filter(email=email, code=code).update(email_verified = True)
        response_data['result'] = 'success'
        response_data['message'] = 'Email Verified.'
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'Email not verified.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def resend_email_verify(request):
    # Old email and new email
    new_email = request.POST['new_email']
    old_email = request.POST['old_email']
    response_data={}
    code = uuid.uuid4()
    if Doctor_profile.objects.filter(email=old_email).exists():
        if not new_email=="null":
            Doctor_profile.objects.filter(email=old_email).update(code = code, email_verified=False, email=new_email)
            link  = settings.SITE_URL+"/v1/api/email_verify?code="+str(code)+"&email="+str(new_email)
            text = str("Hi there,\nPlease click on this "+ link +" to verify your email id.\n\n Thanks")
            print text
            send_email_mandrill("info@piramalcriticalcare.com", new_email, "Email Verification", str(text))
            response_data['result'] = 'success'
            response_data['message'] = 'Verification mail resent.'
        else :
            Doctor_profile.objects.filter(email=old_email).update(code = code, email_verified=False)
            link  = settings.SITE_URL+"/v1/api/email_verify?code="+str(code)+"&email="+str(old_email)
            text = str("Hi there,\nPlease click on this "+ link +" to verify your email id.\n\n Thanks")
            print text
            send_email_mandrill("info@piramalcriticalcare.com", old_email, "Email Verification", str(text))
            response_data['result'] = 'success'
            response_data['message'] = 'Verification mail resent.'
    else : 
        response_data['result'] = 'Failed'
        response_data['message'] = 'Email id not present in the database. Please register.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def forgot_password_email_send(request):
    email = request.POST['email']
    response_data={}
    code = str(uuid.uuid4().fields[-1])[:5]
    # print code
    # print str(uuid.uuid4().fields[-1])[:5]
    if Doctor_profile.objects.filter(email=email).exists():
        Doctor_profile.objects.filter(email=email).update(code = code)
        # link  = settings.SITE_URL+"/v1/api/email_verify?code="+str(code)+"&email="+str(email)
        text = str("Hi there,\nPlease use the below code in the app to change your password.\n"+ str(code) +"\n\n Thanks")
        print text
        send_email_mandrill("info@piramalcriticalcare.com", email, "Password Reset - PiramalAnaethesia", str(text))
        response_data['result'] = 'success'
        response_data['message'] = 'Code has bee sent to your email.'
    else : 
        response_data['result'] = 'Failed'
        response_data['message'] = 'Email id not present in the database. Please register.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def change_password(request):
    response_data={}
    email = request.POST['email']
    password = request.POST['password']
    code = request.POST['code']
    re_password = request.POST['re_password']
    if password==re_password:
        if Doctor_profile.objects.filter(email=email).exists():
            if Doctor_profile.objects.filter(email=email, code=code).exists():
                password = create_MD5_hash(request.POST["password"])
                Doctor_profile.objects.filter(email=email).update(password = password)
                response_data['result'] = 'success'
                response_data['message'] = 'Your password has been successfully changed.'
            else : 
                response_data['result'] = 'Failed'
                response_data['message'] = 'Code is invalid.'
        else : 
            response_data['result'] = 'Failed'
            response_data['message'] = 'Email id is not registered. Please register.'
    else : 
        response_data['result'] = 'Failed'
        response_data['message'] = 'Passwords do not match.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")
###################################Auth##############################


def register(request):
    response_data={}
    if not Access_Code.objects.filter(codes=request.POST["access_code"]).exists() :
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'Invalid access code. Please use another access code.'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    elif Access_Code.objects.filter(codes=request.POST["access_code"]).exists() :
        status_register, message = error_register(request)
        if status_register:
            response_data={}
            response_data['result'] = 'failed'
            response_data['message'] = message
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        else : 
            tempObject = request.POST
            tempObject["password"]=create_MD5_hash(request.POST["password"])
            form = UserMakeForm(tempObject, request.FILES)
            if request.POST["device_id"]:
                if GcmDevice.objects.filter(device_id=request.POST["device_id"], device_type=request.POST["device_type"], is_active=True).exists() :
                    response_data['result'] = 'failed'
                    response_data['message'] = 'Please login as your device is already registered.'
                else : 
                    if form.is_valid():
                        tempForm = form.save(commit=False)
                        tempForm.country_id = Country.objects.get(country=request.POST["country"])
                        tempForm.city = City.objects.get(city=request.POST["city"])
                        if request.FILES["profile_picture"]:
                            tempForm.profile_picture = request.FILES["profile_picture"]
                        tempForm.save()
                        
                        user_id = tempForm.pk
                        user = Doctor_profile.objects.get(_id=user_id)
                        
                        used_code = Access_Code.objects.get(codes=request.POST["access_code"])
                        used_code.user_id = user
                        GcmDevice.objects.create(user=user, device_id=request.POST["device_id"], device_type=request.POST["device_type"], is_active=True)
                        used_code.status = 0
                        used_code.save()
                        # access_token = create_MD5_hash(request.POST["email"]+str(time.time()))
                        # Access_token.objects.create(user_id=user, token_value=access_token, expiry="NULL")
                        # response_data={}
                        # response_data['result'] = 'success'
                        # response_data['access_token'] = access_token
                        # response_data['name'] = user.name
                        response_data['email'] = user.email
                        # response_data['mobile'] = user.mobile
                        # response_data['country_id'] = user.country_id.country
                        # response_data['city'] = user.city.city
                        # response_data['privacy_setting'] = user.privacy_setting
                        # response_data['designation'] = user.designation
                        code = uuid.uuid4()
                        Doctor_profile.objects.filter(email=user.email).update(code = code)
                        link  = settings.SITE_URL+"/v1/api/email_verify?code="+str(code)+"&email="+str(user.email)
                        text = str("Hi "+user.name+ ",\nPlease click on this "+ link +" to verify your email id.\n\n Thanks")
                        send_email_mandrill("info@piramalcriticalcare.com", user.email, "Email Verification", str(text))

                        response_data['result'] = 'success'
                        response_data['message'] = 'Registration of user successful. Please check your email to verify your email id.'
                    else:
                        response_data['result'] = 'failed'
                        response_data['message'] = 'Please fill all the parameters, profile picture as well.'
            else : 
                response_data['result'] = 'failed'
                response_data['message'] = 'Registration of android device failed.'
            return HttpResponse(json.dumps(response_data), content_type="application/json")

def login(request):
    response_data={}
    print request.POST['email']
    user = Doctor_profile.objects.get(email=request.POST['email'])
    if user:
        reg_device_id = GcmDevice.objects.get(user=user, device_type=request.POST["device_type"]).device_id
        print user.password
        if user.password == create_MD5_hash(request.POST["password"]):
            # if reg_device_id==request.POST['device_id']:
            if Doctor_profile.objects.get(email=request.POST['email']).email_verified ==True:

                access_code_to_show = Access_Code.objects.get(user_id=user).codes

                access_token= create_MD5_hash(request.POST["email"]+str(time.time()))
                expiry_date = datetime.datetime.today()+datetime.timedelta (days=30) 
                # right_now = now.strftime("%Y-%m-%d %H:%M") # right_noww = now.replace(day=11, minute=59).strftime('%Y-%m-%d')
                # print now
                Access_token.objects.create(user_id=user, token_value=access_token, expiry=expiry_date)
                response_data={}
                response_data['result'] = 'success'
                response_data['message'] = 'User logged in'
                response_data['access_token'] = access_token
                response_data['name'] = user.name
                response_data['email'] = user.email  
                response_data['mobile'] = user.mobile
                response_data['country_id'] = user.country_id.country
                response_data['city'] = user.city.city
                response_data['privacy_setting'] = user.privacy_setting
                response_data['designation'] = user.designation
                response_data['profile_picture'] = user.profile_picture.name
                response_data['access_code'] = access_code_to_show

                # literature
                user = Doctor_profile.objects.get(email=request.POST["email"])
                country_one = Country.objects.get(country=user.country_id.country)
                country_global = Country.objects.get(country="Global")
                materials = Reading_Material.objects.filter(country=country_one)
                materials_global = Reading_Material.objects.filter(country=country_global)
                merged_materials = []
                for m in materials:
                    merged_materials.append(m)
                for k in materials_global:
                    merged_materials.append(k)

                #events
                user = Doctor_profile.objects.get(email=request.POST["email"])
                country = Country.objects.get(country=user.country_id.country)
                country_global = Country.objects.get(country="global")
                print country_global._id
                events = Event.objects.filter(country__in=[country._id, country_global._id]).order_by('date')
                eventslist = list(set(events))

                # requests
                requests_final_array = []
                receiver = Doctor_profile.objects.get(email=request.POST["email"])
                requests = Connect.objects.filter(receiver_id=receiver, status=0)
                if not len(requests)==0:
                    for request in requests:
                        if request:
                            print request.sender_id.name
                            doctor = Doctor_profile.objects.get(_id=request.sender_id._id)
                            requests_final_array.append(doctor)  

                #notifications
                notifications_finallist = []
                user = Doctor_profile.objects.get(email=request.POST["email"])
                Notifications = Notification.objects.all()
                for user_in_notifs in Notifications:
                    targeted_users_in_notifs = user_in_notifs.targeted_users.all()
                    for user_in_notifss in targeted_users_in_notifs:
                        print user_in_notifss.name
                        if request.POST["email"]==user_in_notifss.email:
                            notifications_finallist.append(user_in_notifs)

                response_data['count_literature'] = len(merged_materials)
                response_data['count_events'] = len(eventslist)
                response_data['count_connect'] = len(requests_final_array)
                response_data['count_notifications'] = len(notifications_finallist)
            else : 
                response_data['result'] = 'failed'
                response_data['message'] = 'User email is not verified.'
            # else:
            #     response_data['result'] = 'failed'
            #     response_data['message'] = 'User mobile is not registered for gcm.'
        else : 
            response_data['result'] = 'failed'
            response_data['message'] = 'Password is incorrect.'
    else :
        response_data['result'] = 'failed'
        response_data['message'] = 'User is not registered.'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def logout(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        tokens = Access_token.objects.filter(user_id=user)
        for token in tokens:
            token.delete()
            print "deleting" + token.token_value
        response_data={}
        response_data['result'] = 'success'
        response_data['message'] = 'User logged out'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")


#########################################################################







##############################CONNECT##############################
def editprofile(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        user.name = request.POST["name"]
        user.new_email = request.POST["new_email"]
        user.mobile = request.POST["mobile"]
        user.country_id = Country.objects.get(country=request.POST["country"])
        user.city = City.objects.get(city=request.POST["city"])
        user.privacy_setting = request.POST["privacy_setting"]
        user.designation = request.POST["designation"]
        user.profile_picture = request.FILES["profile_picture"]
        user.save()
        response_data={}
        response_data['result'] = 'success'
        response_data['message'] = 'User details changed'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

# def changepassword(request):
#     if not len(str(request.POST['new_password'])) >= 8:
#         response_data={}
#         response_data['result'] = 'failed'
#         response_data['message'] = 'Password length should be >=8'
#         return HttpResponse(json.dumps(response_data), content_type="application/json")
#     user = Doctor_profile.objects.get(email=request.POST["email"])
#     password = create_MD5_hash(request.POST["password"])
#     new_password = create_MD5_hash(request.POST["new_password"])
#     print user.password
#     print password
#     if user.password == password :
#         user.password = new_password
#         user.save()
#         response_data={}
#         response_data['result'] = 'success'
#         response_data['message'] = 'Password changed'
#     else:
#         response_data={}
#         response_data['result'] = 'failed'
#         response_data['message'] = 'wrong password'
#     return HttpResponse(json.dumps(response_data), content_type="application/json")

# def changedp(request):
#     if verify_login(request.POST["email"], request.POST["access_token"]):
#         user = Doctor_profile.objects.get(email=request.POST["email"])
#         user.profile_picture = request.FILES["profile_picture"]
#         user.save()
#         response_data={}
#         response_data['result'] = 'success'
#         response_data['message'] = 'Profile picture changed'
#     else:
#         response_data={}
#         response_data['result'] = 'failed'
#         response_data['message'] = 'User not logged in'
#     return HttpResponse(json.dumps(response_data), content_type="application/json")

def getdoctors(request):
    finallist = []
    response_data={}
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country = Country.objects.get(country=request.POST["country"])
        doctors = Doctor_profile.objects.filter(country_id=country)
        requests_receive = Connect.objects.filter(receiver_id=user)
        print len(requests_receive)
        if not len(requests_receive)==0:
            for x in requests_receive:
                if x: 
                    if Doctor_profile.objects.filter(_id=x.sender_id._id, country_id=country).exists():
                        doctor = Doctor_profile.objects.get(_id=x.sender_id._id, country_id=country)
                        if not doctor.email==request.POST["email"]:
                            if x.status==0:
                                doctor.status = "pending"
                                finallist.append(doctor)
                            elif x.status==1:
                                doctor.status = "accepted"
                                finallist.append(doctor)
                            elif x.status==2:
                                doctor.status = "denied"
                                finallist.append(doctor)
                # else : 
                #     response_data['message'] = "There were no requests received."
        requests_send = Connect.objects.filter(sender_id=user)
        print len(requests_send)
        if not len(requests_send)==0:
            for y in requests_send:
                if y : 
                    if Doctor_profile.objects.filter(_id=y.receiver_id._id, country_id=country).exists():
                        doctor = Doctor_profile.objects.get(_id=y.receiver_id._id, country_id=country)
                        print doctor
                        if not doctor in finallist:
                            if y.status==0:
                                doctor.status = "pending"
                                finallist.append(doctor)
                            elif y.status==1:
                                doctor.status = "accepted"
                                finallist.append(doctor)
                            elif y.status==2:
                                doctor.status = "denied"
                                finallist.append(doctor)
                #         else : 
                #             response_data['message'] = "There were no requests."
                # else : 
                #     response_data['message'] = "There were no requests."
        for doctor in doctors:
            if doctor in finallist:
                print "Doctor Already there."
            else :
                if not doctor.email==request.POST["email"]:
                    if doctor.privacy_setting=="hidden":
                        doctor.status = "hidden"
                    elif  doctor.privacy_setting=="maybe":
                        doctor.status = "maybe"
                    elif  doctor.privacy_setting=="show" : 
                        doctor.status = "show"
                    finallist.append(doctor)
        if not len(finallist)==0 :
            response_data= [{'_id': doctor._id,'name': doctor.name,'profile_picture': doctor.profile_picture.name, 'city' : doctor.city.city,'country':doctor.country_id.country, 'privacy_setting':doctor.privacy_setting, 'status':doctor.status } for doctor in finallist ]
        else : 
            response_data=[]
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def sendrequest(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        sender = Doctor_profile.objects.get(email=request.POST["email"])
        receiver = Doctor_profile.objects.get(_id=request.POST["receiver_id"])
        already_sent_request = Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=0)
        if already_sent_request:
            response_data={}
            response_data['message'] = 'Request has already been sent.'
        else:
            already_accepted = Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=1)
            if already_accepted:
                response_data={}
                response_data['message'] = 'The doctor has already accepted your request.'
            else:
                Connect.objects.create(sender_id=sender, receiver_id=receiver, status=0)
                title = "Connect request"
                content = "You have one connect request from sender"+sender.name
                reg_device_id = GcmDevice.objects.get(user=receiver, device_type=request.POST["device_type"]).device_id
                requests = Connect.objects.filter(receiver_id=receiver, status=0)
                length_requests = len(requests)
                send_notification(title, content, length_requests, "connect", reg_device_id)
                # send notification to receiver   You have one connect request from sender. 
                
                response_data={}
                response_data['message'] = 'Request sent to the concerned doctor.'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def acceptrequest(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        sender = Doctor_profile.objects.get(_id=request.POST["sender_id"])
        receiver = Doctor_profile.objects.get(email=request.POST["email"])
        if request.POST["status"]=="denied":
            Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=0).update(status=2)
            title = "Request denied"
            content = "Your connect request to " + str(receiver.name) + " has been denied."
            reg_device_id = GcmDevice.objects.get(user=sender, device_type=request.POST["device_type"]).device_id
            # print title + content + reg_device_id
            send_notification(title, content, "-1", "connect", reg_device_id)
            response_data={}
            response_data['message'] = 'User has denied the request.'
        elif request.POST["status"]=="accept":
            print "Sdsd"
            has_accepted = Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=0).exists()
            has_accepted_two = Connect.objects.filter(sender_id=receiver, receiver_id=sender, status=0).exists()
            print has_accepted_two
            if has_accepted :
                Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=0).update(status=1)
            elif has_accepted_two :
                Connect.objects.filter(sender_id=receiver, receiver_id=sender, status=0).update(status=1)
            title = "Request accepted"
            content = "Your connect request to "+str(receiver.name) +" has been accepted."
            reg_device_id = GcmDevice.objects.get(user=sender, device_type=request.POST["device_type"]).device_id
            # print title + content + reg_device_id
            send_notification(title, content, "-1", "connect", reg_device_id)
            response_data={}
            response_data['message'] = 'User has accepted the request.'
        # particular_request.objects.update(status=1)
        # send notification to sender   Your connect request to receiver has been accepted or denied. 
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def displaydetails(request):
    doctor_final = []
    response_data={}
    if verify_login(request.POST["email"],request.POST["access_token"]):
        doctor = Doctor_profile.objects.get(_id=request.POST["id"])
        receiver = Doctor_profile.objects.get(_id=request.POST["id"])
        sender = Doctor_profile.objects.get(email=request.POST["email"])

        has_accepted = Connect.objects.filter(sender_id=sender, receiver_id=receiver, status=1).exists()
        has_accepted_two = Connect.objects.filter(sender_id=receiver, receiver_id=sender, status=1).exists()
        print has_accepted
        if has_accepted_two or has_accepted:
            doctor_final.append(doctor)
            response_data=json.dumps( [{'_id': doctor._id, 'name': doctor.name, 'profile_picture': doctor.profile_picture.name, 'city' : doctor.city.city, 'country':doctor.country_id.country, 'designation':doctor.designation, 'profile_picture': doctor.profile_picture.name, 'email':doctor.email, 'new_email':doctor.new_email, 'mobile':doctor.mobile }for doctor in doctor_final])
            return HttpResponse(response_data, content_type="application/json")
        elif doctor.privacy_setting=="show": 
            if not len(doctor_final) > 0:
                doctor_final.append(doctor)
                response_data= json.dumps([{'_id': doctor._id, 'name': doctor.name, 'profile_picture': doctor.profile_picture.name, 'city' : doctor.city.city, 'country':doctor.country_id.country, 'designation':doctor.designation, 'profile_picture': doctor.profile_picture.name, 'email':doctor.email, 'new_email':doctor.new_email, 'mobile':doctor.mobile }for doctor in doctor_final])
                return HttpResponse(response_data, content_type="application/json")
        elif doctor.privacy_setting=="maybe":
            response_data['result'] = 'failed'
            response_data['message'] = 'Ask user permission.'
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        elif doctor.privacy_setting=="denied":
            response_data['result'] = 'failed'
            response_data['message'] = 'User has denied all requests.'
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

def displayrequests(request):
    finallist = []
    response_data={}
    if verify_login(request.POST["email"], request.POST["access_token"]):
        receiver = Doctor_profile.objects.get(email=request.POST["email"])
        requests = Connect.objects.filter(receiver_id=receiver, status=0)
        if not len(requests)==0:
            for request in requests:
                # print request
                if request:
                    print request.sender_id.name
                    doctor = Doctor_profile.objects.get(_id=request.sender_id._id)
                    # print doctor
                    finallist.append(doctor)                    
            response_data=[{'_id': doctor._id, 'name': doctor.name, 'profile_picture': doctor.profile_picture.name, 'city' : doctor.city.city, 'country':doctor.country_id.country,'privacy_setting':doctor.privacy_setting } for doctor in finallist ]
        else : 
            response_data=[]
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")


####################################################################################################















#####################About US - Testimonials#####################

def addtestimonial(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        Testimonial.objects.create(user_id=user, country = user.country_id, testimonial_content=request.POST["testimonial_content"])
        response_data={}
        response_data['result'] = 'success'
        response_data['message'] = 'Testimonial added'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def gettestimonials(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        testimonials_user = Testimonial.objects.filter(status="approved").order_by('-timestamp')[request.POST["start_count"]:request.POST["end_count"]]

        response_data={}
        response_data=json.dumps( [{'testimonial_content':testimonial.testimonial_content, 'timestamp':str(testimonial.timestamp), 'user':testimonial.user_id.name, 'country':testimonial.user_id.country_id.country, 'city':testimonial.user_id.city.city, 'profile_picture': testimonial.user_id.profile_picture.name} for testimonial in testimonials_user])  # start from 0 [0:]
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(response_data, content_type="application/json")

def get_fixed_testimonials(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        fixed_testimonial = Fixed_testimonial.objects.all().order_by('-timestamp')[request.POST["start_count"]:request.POST["end_count"]]
        response_data={}
        response_data=json.dumps( [{'testimonial_content':testimonial.testimonial_content, 'user':testimonial.name, 'designation':testimonial.designation} for testimonial in fixed_testimonial ])  # start from 0 [0:]
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(response_data, content_type="application/json")

##########################################################################################3
















####################################Get in touch#######################################
def contact_request(request):
    finalist = []
    response_data={}

    if verify_login(request.POST["email"], request.POST["access_token"]):
        country = Country.objects.get(country=request.POST["country"])
        roles = ""
        all_contacts = Contactus.objects.all()
        print all_contacts
        for x in all_contacts : 
            print x.country.all()
            for y in x.country.all() : 
                if str(y)==request.POST["country"] :
                    print str(y)
                    print len (x.Role.all())
                    
                    for rolwa in x.Role.all():
                        roles = roles+"|"+str(rolwa)
                    x.roles = roles
                    finalist.append(x)

        response_data=json.dumps( [{'name':contact.Name, 'emailid':contact.EmailId, 'role':roles} for contact in finalist ]) 
        return HttpResponse(response_data, content_type="application/json")
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

def contact_request_submit(request):
    response_data={}
    countries = []
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country = Country.objects.get(country=request.POST["country"])
        emailid = request.POST["emailid"]
        role_post = request.POST["role"];
        contactus_category = Contactus_categories.objects.get(Category=role_post)
        requesttt = Contactus.objects.filter(EmailId=emailid)
        for one in requesttt:
            all_roles = one.Role.all()
            for corresponding_role in all_roles :
                if str(corresponding_role)==role_post :
                    people_contacted = one.Number_of_people_contacted
                    if not len(one.people.all())==0 :
                        if not len(Contact_Queries.objects.filter(Contact=one, Query_type=contactus_category))==0:
                        # for userr in one.people.all():
                            response_data['result'] = 'failed'
                            response_data['message'] = 'You have already submitted the request. The concened person will contact you as soon as possible.'
                        else:
                            one.Number_of_people_contacted = people_contacted+1
                            countries.append(country)
                            Contact_Queries.objects.create(Contact=one, Query_type=contactus_category, Doctor_user=user)
                            query_just_saved =  Contact_Queries.objects.get(Contact=one, Query_type=contactus_category, Doctor_user=user)
                            query_just_saved.country = countries
                            query_just_saved.save()
                            one.people.add(user)
                            one.save()
                            response_data['result'] = 'success'
                            response_data['message'] = 'The concerned authority will get in touch with you at the earliest.'

                            doctor_user = Doctor_profile.objects.get(email=request.POST["email"])
                            doctor_user_email = doctor_user.email
                            doctor_user_name = doctor_user.name
                            subject = str(contactus_category.Category) + "- Query"
                            text = str("Hi there,"+"\n"+doctor_user_name+", have some queries regarding the topic "+contactus_category.Category+ ". Please get in touch with the doctor.\n\nThankYou")
                            print text
                            send_email_mandrill(doctor_user_email, emailid, subject, str(text))
                    else : 
                        one.Number_of_people_contacted = people_contacted+1
                        one.people.add(user)
                        countries.append(country)
                        Contact_Queries.objects.create(Contact=one, Query_type=contactus_category, Doctor_user=user)
                        query_just_saved =  Contact_Queries.objects.get(Contact=one, Query_type=contactus_category, Doctor_user=user)
                        query_just_saved.country = countries
                        query_just_saved.save()
                        one.people.add(user)
                        one.save()
                        response_data['result'] = 'success'
                        response_data['message'] = 'The concerned authority will get in touch with you at the earliest.' 

                        doctor_user = Doctor_profile.objects.get(email=email)
                        doctor_user_email = doctor_user.email
                        doctor_user_name = doctor_user.name
                        subject = str(contactus_category.Category) + "- Query"
                        text = str("Hi there,"+"\n"+doctor_user_name+", have some queries regarding the topic "+contactus_category.Category+ ". Please get in touch with the doctor.\n\nThankYou")
                        print text
                        send_email_mandrill(doctor_user_email, emailid, subject, str(text))
                else:
                    response_data['result'] = 'failed'
                    response_data['message'] = 'User not logged in'
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")
##########################################################################














#############################EVENTS########################################3

def getevents(request):
    response_data={}
    if verify_login(request.POST["email"],request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country = Country.objects.get(country=request.POST["country"])
        country_global = Country.objects.get(country="global")
        print country_global._id
        events = Event.objects.filter(country__in=[country._id, country_global._id]).order_by('date')[request.POST["start_count"]:request.POST["end_count"]]  
        eventslist = list(set(events))    
        response_data=json.dumps( [{'event_id': event.event_id, 'date':str(event.date),'title':event.title,'description':event.description,'venue':event.venue,'timings':str(event.timings),'speaker':event.speaker, 'footfall':event.footfall,'total_going':event.attending_users.all().count(),'going_status': user in event.attending_users.all() } for event in eventslist ])
        return HttpResponse(response_data, content_type="application/json")
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

def change_event_status(request):
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        event = Event.objects.get(event_id=request.POST["event_id"])  #single response
        response_data={}
        if request.POST["going_status"] == "1":
            Event.objects.filter(event_id=request.POST["event_id"]).update(footfall = event.footfall+1)
            event.attending_users.add(user)
            event.save()
            response_data['message'] = 'User going-status changed to yes'
        elif request.POST["going_status"] == "0":
            for userr in event.attending_users.all():
                if userr==user :
                    Event.objects.filter(event_id=request.POST["event_id"]).update(footfall = event.footfall-1)
            event.attending_users.remove(user)
            event.save()
            response_data['message'] = 'User going-status changed to no'
            response_data['result'] = 'success'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")


##############################################################################3
















############################Reading Materials##################


def getReadingMaterial(request):
    response_data={}
    if verify_login(request.POST["email"], request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])

        country_one = Country.objects.get(country=request.POST["country"])
        country_global = Country.objects.get(country="Global")

        merged_materials = []
        materials = Reading_Material.objects.all()
        for y in materials :
            country_sss = y.country.all()
            print country_sss
            if country_one in country_sss :
                print "yes"
                merged_materials.append(y)
            if country_global in country_sss :
                merged_materials.append(y)

        merged_materials = list(set(merged_materials))
        response_data=json.dumps( [{'heading': material.heading,'weblink': material.pdf_data.name, 'type_of_material' : material.type_of_material } for material in merged_materials ])
        return HttpResponse(response_data, content_type="application/json")
    else:
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
        return HttpResponse(json.dumps(response_data), content_type="application/json")


#######################################################################

















###############################SURVEY#########################
def getsurveyquestions(request):
    if verify_login(request.POST["email"],request.POST["access_token"]):
        finallist = []
        user = Doctor_profile.objects.get(email=request.POST["email"])

        country_one = Country.objects.get(country=request.POST["country"])
        country_global = Country.objects.get(country="Global")
        # surveys = Survey_question.objects.filter(country=country)
        # surveys_global = Survey_question.objects.filter(country=country_global)
        surveys_one = []
        surveys_global = []
        surveys = Survey_question.objects.all()
        for y in surveys :
            country_sss = y.country.all()
            print country_sss
            if country_one in country_sss :
                surveys_one.append(y)
            if country_global in country_sss :
                surveys_global.append(y)

        for survey in surveys_one:
            already_responded = Survey_Responses.objects.filter(user_id=user, question=survey)
            if len(finallist) < 2:
                if not already_responded:
                    targeted_users_in_surveys = survey.targeted_users.all()
                    for user_in_survey in targeted_users_in_surveys:
                        if request.POST["email"]==user_in_survey.email:
                            finallist.append(survey)
            else:
                break
        for survey in surveys_global:
            already_responded = Survey_Responses.objects.filter(user_id=user, question=survey)
            if len(finallist) < 2:
                if not already_responded:
                    targeted_users_in_surveys = survey.targeted_users.all()
                    for user_in_survey in targeted_users_in_surveys:
                        if request.POST["email"]==user_in_survey.email:
                            finallist.append(survey)
            else :
                break
        response_data={}
        response_data=json.dumps( [{'id': survey._id, 'question': survey.question} for survey in finallist ])
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(response_data, content_type="application/json")

def getsurveyoptions(request):
    if verify_login(request.POST["email"],request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        survey = Survey_question.objects.get(_id=request.POST["id"])
        options = survey.survey_values_set.all()
        response_data={}
        response_data=json.dumps( [{'option': option.option} for option in options ])
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(response_data, content_type="application/json")

def submitsurvey(request):
    one_split = []
    two_split = []
    if verify_login(request.POST["email"],request.POST["access_token"]):
        user = Doctor_profile.objects.get(email=request.POST["email"])
        country_one = Country.objects.get(country=request.POST["country"])

        user_country =[]
        user_country.append(country_one)
        one = request.POST["one"]
        one_split = one.split('-', 1 )
        survey_one = Survey_question.objects.get(_id=one_split[0])
        option_one = Survey_values.objects.get(option=one_split[1])
        one_created = Survey_Responses.objects.create(user_id=user, question=survey_one, value=option_one)
        one_created.country.add(country_one)
        one_created.save()

        if not request.POST["two"]=="-1":
            two = request.POST["two"]
            two_split = two.split('-', 1 )
            survey_two = Survey_question.objects.get(_id=two_split[0])
            option_two = Survey_values.objects.get(option=two_split[1])
            two_created = Survey_Responses.objects.create(user_id=user, question=survey_two, value=option_two)
            two_created.country.add(country_one)
            two_created.save()
        response_data={}
        response_data['result'] = 'success'
        response_data['message'] = 'Response has been recorded.'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")
####################################################################3
















###############################Notifications#########################
def getnotification(request):
    if verify_login(request.POST["email"],request.POST["access_token"]):
        finallist = []
        user = Doctor_profile.objects.get(email=request.POST["email"])
        # country = Country.objects.get(country=request.POST["country"])
        Notifications = Notification.objects.all()


        for user_in_notifs in Notifications:
            targeted_users_in_notifs = user_in_notifs.targeted_users.all()
            for user_in_notifss in targeted_users_in_notifs:
                print user_in_notifss.name
                if request.POST["email"]==user_in_notifss.email:
                    finallist.append(user_in_notifs)
        response_data={}
        response_data=json.dumps( [{'title': notif.title, 'message': notif.content} for notif in finallist ])
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(response_data, content_type="application/json")

def notif_regid_change(request):
    if verify_login(request.POST["email"],request.POST["access_token"]):
        if request.POST["device_id"]:
            user = Doctor_profile.objects.get(email=request.POST["email"])
            GcmDevice.objects.filter(user=user, device_type=request.POST["device_type"]).update(device_id=request.POST["device_id"])
            response_data={}
            response_data['result'] = 'success'
            response_data['message'] = 'Mobile id changed.'
    else:
        response_data={}
        response_data['result'] = 'failed'
        response_data['message'] = 'User not logged in'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

####################################################################3
