from django.contrib.auth.models import User
from adminportal.models import *
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource

class MyAuthentication(BasicAuthentication):
    def __init__(self, *args, **kwargs):
        super(MyAuthentication, self).__init__(*args, **kwargs)

    def is_authenticated(self, request, **kwargs):
        return True

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'auth/user'
        excludes = ['email', 'password', 'is_superuser']

        authentication = MultiAuthentication(BasicAuthentication(), ApiKeyAuthentication())
        authorization = DjangoAuthorization()
        
class Event(ModelResource):
    class Meta:
        queryset = Event.objects.all()
        resource_name = 'event'
        
        authentication = MyAuthentication()
        
    
class City(ModelResource):
    class Meta:
        queryset = City.objects.all()
        resource_name = 'city'
        
        authentication = MyAuthentication()
        
    