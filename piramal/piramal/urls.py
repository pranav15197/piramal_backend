
from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.contrib import admin
from djrill import DjrillAdminSite

admin.site = DjrillAdminSite()
admin.autodiscover()

from adminportal import views, urls
from tastypie.api import Api
from piramal.api import *
from django.conf.urls.static import static
from django.conf import settings

# from adminportal.admin import admin_site

v1_api = Api(api_name='v1')
v1_api.register(Event())
v1_api.register(City())

urlpatterns = [
	url(r'^', include(urls)),
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
	# url(r'^myadmin/', include(admin_site.urls)),
    url(r'^admin/', include(admin.site.urls)), # admin site
    url(r'^v1/api/', include(urls)), # API to call http://127.0.0.1:8000/api/v1/product/1/?format=json
    # url(r'', include('gcm.urls')),
    # url(r'^$', include('adminportal.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        
